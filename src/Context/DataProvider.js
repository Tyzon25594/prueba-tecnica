import React,{ useState,useEffect,createContext } from "react";
import axios from 'axios';

export const DataContext= createContext();

export const DataProvider =(props)=>{
  const baseUrl="https://localhost:44304/api/producto"
  const [data,setData]=useState([])
  const [carrito,setCarrito]=useState([])
  const [total,setTotal]=useState(0)
  const [agregado,setAgregado]=useState(false)
  
  const peticionGet=async()=>{ 
    await axios.get(baseUrl)
    .then(response=>{ setData(response.data);
      console.log(data)
    }).catch(error=>{
       console.log(error); 
      })
  }
 useEffect(()=>{
 peticionGet();},[])

 const addCarrito=(id)=>{
     const check=carrito.every(item=>{
         return item.id!==id;
     })
     if(check){
         const dataPro=data.filter(producto=>{
             return producto.id===id
         })
         setCarrito([...carrito,...dataPro])
     }else{
         alert("El producto ya se encuentra agregado al carrito")
     }
 }
 const validar=(id)=>{
    const check=carrito.every(item=>{
        return item.id!==id;
    })
    if(check){
        return false
    }else{
        return true
    }
}
 useEffect(()=>{
     const getTotal=()=>{
         const res = carrito.reduce((prev,item)=>{
             console.log(item.precio)
             setTotal(prev + item.precio)
             return prev + item.precio;
         },0)
     }
     getTotal()
 },[carrito])
 const value={
     productos:[data],
     addCarrito:addCarrito,
     carrito:[carrito,setCarrito],
     total:[total,setTotal],
     validar:validar
 }
 console.log(value)
 return(
     <DataContext.Provider value={value}>
         {props.children}
     </DataContext.Provider>
 )

}