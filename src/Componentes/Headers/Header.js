import * as React from 'react';
import {Button, AppBar, Toolbar, Typography,Badge,IconButton,Grid } from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import * as ProductoService from '../../servicios/ProductoService';
import Menu from '@material-ui/core/Menu';
import {DataContext} from '../../Context/DataProvider'
import Carrito from '../../Componentes/Carrito/Carrito'

const useStyles = makeStyles(theme => ({
  offset: theme.mixins.toolbar,
  title:{
    flexGrow:1,
  }
}))

function Header() {
  const clases=useStyles()
  const value=React.useContext(DataContext)
  const [carrito,setCarrito]=value.carrito
  const [total]=value.total
  console.log(carrito)
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const removeAll=()=>{
    while(carrito.length > 0){  
        carrito.pop(); 
        setCarrito([...carrito])
    }
    
  }

  return (
    <div >
      <AppBar position="fixed" color="primary">
        <Toolbar>
          <Typography variant="h6" className={clases.title}>
            TIENDA
          </Typography>
          <IconButton onClick={handleClick}>
            <Badge badgeContent={carrito.length} color="primary">
              <AddShoppingCartIcon fontSize="large" />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: 'visible',
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            minWidth:350,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            '&:before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
       {  
        carrito.length===0 ?<Grid sx={{textAlign:"center", color: 'text.primary' }} fullWidth item xs={12}> <h4  styles={{
          textAlign:"center", fontSize:"3rem"
        }}> Carrito vacio</h4></Grid>:<div>
        <Grid container sx={{ color: 'text.primary',ml:6 }} fullWidth>
        <Grid item xs={6}>
      </Grid>
        <Grid item xs={6}>
        <Button size="small" fullWidth sx={{mb:1}}  onClick={()=>removeAll()} >Vaciar</Button>
      </Grid>
        {carrito.map(car=>
       <Carrito car={car} key={car.id}/>
        
         )}
         <Grid item xs={6}>
         <Typography sx={{mt:2}} > Total:  {total} S/.</Typography>
         </Grid>
        </Grid>
        </div>
       }
      </Menu>
      <div className={clases.offset}> </div>
    </div>
   
  );
}

export default Header;
