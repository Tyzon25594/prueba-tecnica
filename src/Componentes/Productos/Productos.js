import React,{useState,useEffect,useContext} from 'react';
import Producto from './Producto';
import { Grid, } from '@material-ui/core';
import {DataContext} from '../../Context/DataProvider'

function Productos(props) {
  const value=useContext(DataContext)
  const [productos]=value.productos
  return (
    <Grid container spacing={2}>
    {productos.map(producto=><Producto producto={producto} key={producto.id}/>)  }
    </Grid>
  );
}

export default Productos;
