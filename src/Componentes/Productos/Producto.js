import  React,{useContext, useState} from 'react';
import {Button,CardMedia,CardContent,Card,CardActions,Typography,Grid} from '@material-ui/core';
import DetalleProducto from './DetalleProducto';
import * as ProductoService from '../../servicios/ProductoService' 
import PropTypes from 'prop-types';
import {DataContext} from '../../Context/DataProvider'
//const [openPopup, setOpenPopup] = useState(false)



function Producto(props) {
  const value=useContext(DataContext)
  const addCarrito=value.addCarrito
  const validar=value.validar
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState([]);
  console.log(() => validar(props.producto.id) );
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };

   
    const agregar=(producto,e)=>{
      console.log(producto)
      ProductoService.generateCarritoId()
      
  };
    
        return (
            <>
            <Card sx={{ maxWidth: 250,margin:4,minHeight:200,maxHeight:300,minWidth:250 }}>
      <CardMedia
        component="img"
        height="140"
        image={props.producto.url}
        alt="green iguana"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.producto.nombre}
        </Typography>
        <Typography variant="body2" color="text.secondary">
        {props.producto.descripcion}
          
        </Typography>
        <Typography variant="body2" color="text.secondary">
        {props.producto.precio}
          
        </Typography>
      </CardContent>
      <CardActions>
        
         
        <Button size="small" onClick={handleClickOpen}>Visualizar</Button>
        <Button size="small" onClick={() => addCarrito(props.producto.id)}>Agregar</Button>
      </CardActions>
    </Card>
    <DetalleProducto  producto={props.producto} 
      selectedValue={selectedValue}
      open={open}
      onClose={handleClose}
    />
    </>
         );
  }


export default Producto;
