
import React,{useContext} from 'react'
import { Dialog, DialogTitle, DialogContent, Typography,Grid} from '@material-ui/core';
import {Button,CardMedia,CardContent,Card,CardActions} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {DataContext} from '../../Context/DataProvider'


const useStyles = makeStyles(theme => ({
    dialogWrapper: {
        padding: theme.spacing(2),
        position: 'absolute',
        top: theme.spacing(5)
    },
    dialogTitle: {
        paddingRight: '0px'
    }
}))

export default function DetalleProducto(props) {

  const value=useContext(DataContext)
  const addCarrito=value.addCarrito

    const classes = useStyles();

    const { onClose, selectedValue, open } = props;

    const handleClose = () => {
      onClose(selectedValue);
    };
  
    const handleListItemClick = (value) => {
      onClose(value);
    };
  
  
    return (
        <Dialog onClose={handleClose} open={open} maxWidth="md" classes={{ paper: classes.dialogWrapper }} >
            
            
            <Card sx={{ maxWidth: 350,minHeight:420,maxHeight:420,minWidth:350 }}>
      <CardMedia
        component="img"
        height="200"
        image={props.producto.url}
        alt="green iguana"
      />
      <CardContent>
        <Typography  variant="h5" >
          {props.producto.nombre}
        </Typography>
        <Typography variant="body2" color="text.secondary">
        {props.producto.descripcion}
          
        </Typography>
        <Typography sx={{mt:2}}  variant="h6" >
        Iformación extra
        </Typography>
        <Typography sx={{mt:0,pt:0}} variant="body2"  color="text.secondary">
        
        {props.producto.informacion.toLowerCase().charAt(0).toUpperCase() + props.producto.informacion.toLowerCase().slice(1)}
          
        </Typography>
      </CardContent>
      <CardActions>
        <Grid item xs={12} sx={{textAlign:"right"}}>
        <Button size="small" onClick={() => addCarrito(props.producto.id)} >Agregar</Button>
        </Grid>
      </CardActions>
    </Card>
        </Dialog>
    )
}
