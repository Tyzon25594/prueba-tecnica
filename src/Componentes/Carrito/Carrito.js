import React,{useState,useEffect,useContext,Typography} from 'react';
import {DataContext} from '../../Context/DataProvider'
import {Grid,IconButton } from '@material-ui/core';
import DeleteIcon  from '@material-ui/icons//Delete';

function Carrito(props) {
  const value=useContext(DataContext)
  const [productos]=value.productos
  const [carrito,setCarrito]=value.carrito
  
  const removeProducto=id=>{
    carrito.forEach((item,index)=>{
      if(item.id==id){
        carrito.splice(index,1)
      }
    })
    setCarrito([...carrito])
  }
  return (
    <>
    <Grid item xs={6} >
    {props.car.nombre}
      </Grid>
      
        <Grid item xs={5} >
      <IconButton sx={{m:0,p:0}} onClick={()=>removeProducto(props.car.id)}>
        <DeleteIcon sx={{m:0,p:0}} />
      </IconButton>
      </Grid>
      
        </>
  );
}

export default Carrito;
