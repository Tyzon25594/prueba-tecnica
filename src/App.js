import React,{useState,useEffect} from 'react';
import './App.css';
import {createTheme} from '@material-ui/core';
import {ThemeProvider } from '@material-ui/styles';
import Header from './Componentes/Headers/Header';
import Productos from './Componentes/Productos/Productos';
import { DataProvider } from './Context/DataProvider';
const theme = createTheme()

function App() {
  
  return (
    <DataProvider>
    <ThemeProvider theme={theme}>
      <Header></Header>
      <Productos ></Productos>
    </ThemeProvider>
    </DataProvider>
  );
}

export default App;
